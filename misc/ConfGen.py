import numpy as np
import h5py
import argparse
from hilbertcurve import HilbertCurve

parser = argparse.ArgumentParser()
parser.add_argument('-o',
            action="store", dest="name", required = True,
            help="name for output file")
arguments = parser.parse_args()

Ndim = 3
power = 2 # adjust power for different chain discretiation N = 2**(Ndim * power)
rho = 0.85
Ncp = 10 # adjust number of polymer per edge of box, i.e. n_polys = Ncp**3

N = 2**(Ndim * power)
scale = rho**(-1. / 3.)
l0 = 2**power * scale
n_polys = Ncp**3

hilbert_curve = HilbertCurve(power, Ndim)
distances = list(range(N))
r = 0.965 * np.array(hilbert_curve.points_from_distances(distances))

bonds = r - np.roll(r, -1, axis = 0)
bonds = bonds[:N - 1, :]
bonds = np.sqrt(np.sum(bonds * bonds, axis = 1))
max_bond = np.amax(bonds)
print('maximal bond length = {}'.format(max_bond))

n_beads = n_polys * N
bl_len_max = 2
Lx = Ly = Lz = l0 * Ncp
time = 0.

rx = np.zeros(n_beads)
ry = np.zeros(n_beads)
rz = np.zeros(n_beads)
imx = np.zeros(n_beads, dtype=int)
imy = np.zeros(n_beads, dtype=int)
imz = np.zeros(n_beads, dtype=int)
px = np.zeros(n_beads)
py = np.zeros(n_beads)
pz = np.zeros(n_beads)

x0 = l0 * np.array(range(Ncp))
x, y, z = np.meshgrid(x0, x0, x0)
poly_shift = np.vstack([x.ravel(), y.ravel(), z.ravel()])
poly_shift = poly_shift.T
poly_shift = np.repeat(poly_shift[:, np.newaxis, :], N, axis = 1)

r = np.repeat(r[np.newaxis, :, :], n_polys, axis = 0)

r = r + poly_shift

rx = np.ravel(r[:, :, 0])
ry = np.ravel(r[:, :, 1])
rz = np.ravel(r[:, :, 2])

poly_offset = N * np.array(range(n_polys))
poly_N = np.repeat(N, n_polys)

bl_len = np.zeros(N, dtype = int)
bl_len[:] = 2
bl_len[0] = 1
bl_len[N - 1] = 1

bl = np.zeros((N, 2))
mono = np.array(range(N))
bl[:, 0] = mono - 1
bl[:, 1] = mono + 1
bl[0, 0] = 1
bl[N - 1, 0] = N - 2

bl_len = np.repeat(bl_len[np.newaxis, :], n_polys, axis = 0)
bl = np.repeat(bl[np.newaxis, :, :], n_polys, axis = 0)

bl_len = bl_len.reshape((n_beads))
bl_shift = np.repeat(poly_offset[:, np.newaxis], N, axis = 1)
bl_shift = np.repeat(bl_shift[:, :, np.newaxis], 2, axis = 2)
bl = bl + bl_shift
bl = bl.reshape((n_beads, 2))


f = h5py.File(arguments.name, 'w')
f.create_dataset("rx", dtype=np.double, data = rx)
f.create_dataset("ry", dtype=np.double, data = ry)
f.create_dataset("rz", dtype=np.double, data = rz)
f.create_dataset("imx", dtype=np.intc, data = imx)
f.create_dataset("imy", dtype=np.intc, data = imy)
f.create_dataset("imz", dtype=np.intc, data = imz)
f.create_dataset("px", dtype=np.double, data = px)
f.create_dataset("py", dtype=np.double, data = py)
f.create_dataset("pz", dtype=np.double, data = pz)
f.create_dataset("bl", dtype=np.intc, data = bl)
f.create_dataset("bl_len", dtype=np.intc, data = bl_len)
f.create_dataset("poly_offset", dtype=np.intc, data = poly_offset)
f.create_dataset("poly_N", dtype=np.intc, data = poly_N)
f.create_dataset("n_beads", dtype=np.intc, data = n_beads)
f.create_dataset("n_polys", dtype=np.intc, data = n_polys)
f.create_dataset("bl_len_max", dtype=np.intc, data = bl_len_max)
f.create_dataset("Lx", dtype=np.double, data = Lx)
f.create_dataset("Ly", dtype=np.double, data = Ly)
f.create_dataset("Lz", dtype=np.double, data = Lz)
f.create_dataset("time", dtype=np.double, data = time)

f.close()
