#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#ifdef _OPENMP
#include <omp.h>
#endif
#ifdef _OPENACC
#include <openacc.h>
#endif
#include "system.h"
#include "io.h"
#include "pcg.h"
#include "integrate.h"
#include "ana.h"
#include "mesh.h"

volatile int NoKeyboardInterrupt = 1;

void intHandler(int dummy)
{
    printf("Catched Signal to Stop.\n");
    NoKeyboardInterrupt = 0;
}

int initialize(MySystem * s, int argc, char *argv[])
{
    signal(SIGTSTP, intHandler);
    if(argc - 1 != 10)
    {
        printf("USAGE: ./run output-prefix conf-file ana-file-name simulation-time dt r_buffer info_dt ana_dt_double conf_save_dt_double walltime (%d / 10)\n", argc - 1);
        return -1;
    }

    s->prefix = argv[1];
    s->conf_file_name = argv[2];
    s->ana_file_name = argv[3];
    s->sim_time_double = atof(argv[4]);
    s->dt = atof(argv[5]);
    const double r_buffer = atof(argv[6]);
    s->r_buffer2 = r_buffer * r_buffer;
    s->info_dt = atof(argv[7]);
    s->ana_dt_double = atof(argv[8]);
    s->conf_save_dt_double = atof(argv[9]);
    s->walltime = atof(argv[10]);

    read_conf_scalars(s);
    init_mesh(s);

    printf("########################################################################################################################\n");
    printf("Simulation parameters:\n");
    printf("n_beads %d n_cells %d n_polys %d bl_len_max %d time %lg r_cut %lg r_buffer %lg lx %lg ly %lg lz %lg dt %lg conf_save_dt_double %lg ana_dt_double %lg info_dt %lg walltime %d\n", s->n_beads, s->n_cells, s->n_polys, s->bl_len_max, s->time, sqrt(s->r_cut2), sqrt(s->r_buffer2), s->lx, s->ly, s->lz, s->dt, s->conf_save_dt_double, s->ana_dt_double, s->info_dt, s->walltime);

    s->sim_time_int = (int)nearbyint(s->sim_time_double / s->dt);
    s->conf_save_dt_int = (int)nearbyint(s->conf_save_dt_double / s->dt);
    s->ana_dt_int = (int)nearbyint(s->ana_dt_double / s->dt);

    int n_beads = s->n_beads;
    int n_polys = s->n_polys;
    int n_cells = s->n_cells;
    s->cl_len_max = 1;
    s->n_rngs = s->n_beads;

    s->rx = (double*)malloc(n_beads * sizeof(double));
    s->ry = (double*)malloc(n_beads * sizeof(double));
    s->rz = (double*)malloc(n_beads * sizeof(double));
    s->imx = (int*)malloc(n_beads * sizeof(int));
    s->imy = (int*)malloc(n_beads * sizeof(int));
    s->imz = (int*)malloc(n_beads * sizeof(int));
    s->rx0 = (double*)malloc(n_beads * sizeof(double));
    s->ry0 = (double*)malloc(n_beads * sizeof(double));
    s->rz0 = (double*)malloc(n_beads * sizeof(double));
    s->imx0 = (int*)malloc(n_beads * sizeof(int));
    s->imy0 = (int*)malloc(n_beads * sizeof(int));
    s->imz0 = (int*)malloc(n_beads * sizeof(int));
    s->rx_check = (double*)malloc(n_beads * sizeof(double));
    s->ry_check = (double*)malloc(n_beads * sizeof(double));
    s->rz_check = (double*)malloc(n_beads * sizeof(double));
    s->imx_check = (int*)malloc(n_beads * sizeof(int));
    s->imy_check = (int*)malloc(n_beads * sizeof(int));
    s->imz_check = (int*)malloc(n_beads * sizeof(int));
    s->px = (double*)malloc(n_beads * sizeof(double));
    s->py = (double*)malloc(n_beads * sizeof(double));
    s->pz = (double*)malloc(n_beads * sizeof(double));
    s->fx = (double*)malloc(n_beads * sizeof(double));
    s->fy = (double*)malloc(n_beads * sizeof(double));
    s->fz = (double*)malloc(n_beads * sizeof(double));
    s->bl = (int*)malloc(n_beads * s->bl_len_max * sizeof(int));
    s->nl = (int*)malloc(n_beads * 27 * s->cl_len_max * sizeof(int));
    s->bl_len = (int*)malloc(n_beads * sizeof(int));
    s->nl_len = (int*)malloc(n_beads * sizeof(int));
    s->cell_neighbor = (int*)malloc(n_cells * 27 * sizeof(int));
    s->poly_offset = (int*)malloc(n_polys * sizeof(int));
    s->poly_N = (int*)malloc(n_polys * sizeof(int));
    s->T_k = (double*)malloc(n_beads * sizeof(double));
    s->F2 = (double*)malloc(n_beads * sizeof(double));
    s->nablaF = (double*)malloc(n_beads * sizeof(double));
    s->id_cell = (int*)malloc(n_beads * sizeof(int));
    s->pcgstate = (PCG_STATE*)malloc(s->n_rngs * sizeof(PCG_STATE));

    s->cell_begin = (int*)malloc(n_cells * sizeof(int));
    s->cell_end = (int*)malloc(n_cells * sizeof(int));
    s->id_orig = (int*)malloc(n_beads * sizeof(int));
    s->id_orig_rev = (int*)malloc(n_beads * sizeof(int));
    s->id_sort = (int*)malloc(n_beads * sizeof(int));
    s->id_unsort = (int*)malloc(n_beads * sizeof(int));

    s->rx_copy = (double*)malloc(n_beads * sizeof(double));
    s->ry_copy = (double*)malloc(n_beads * sizeof(double));
    s->rz_copy = (double*)malloc(n_beads * sizeof(double));
    s->imx_copy = (int*)malloc(n_beads * sizeof(int));
    s->imy_copy = (int*)malloc(n_beads * sizeof(int));
    s->imz_copy = (int*)malloc(n_beads * sizeof(int));
    s->rx0_copy = (double*)malloc(n_beads * sizeof(double));
    s->ry0_copy = (double*)malloc(n_beads * sizeof(double));
    s->rz0_copy = (double*)malloc(n_beads * sizeof(double));
    s->imx0_copy = (int*)malloc(n_beads * sizeof(int));
    s->imy0_copy = (int*)malloc(n_beads * sizeof(int));
    s->imz0_copy = (int*)malloc(n_beads * sizeof(int));
    s->px_copy = (double*)malloc(n_beads * sizeof(double));
    s->py_copy = (double*)malloc(n_beads * sizeof(double));
    s->pz_copy = (double*)malloc(n_beads * sizeof(double));
    s->fx_copy = (double*)malloc(n_beads * sizeof(double));
    s->fy_copy = (double*)malloc(n_beads * sizeof(double));
    s->fz_copy = (double*)malloc(n_beads * sizeof(double));
    s->bl_copy = (int*)malloc(n_beads * s->bl_len_max * sizeof(int));
    s->bl_len_copy = (int*)malloc(n_beads * sizeof(int));
    s->id_cell_copy = (int*)malloc(n_beads * sizeof(int));
    s->id_orig_copy = (int*)malloc(n_beads * sizeof(int));

    // initialize random number generators
    s->pcgs_main = &s->pcgstate_main;
    seed_pcg32(s->pcgs_main, time(NULL), 1);

    for(int i = 0; i < s->n_rngs; i++)
    {
        seed_pcg32(&s->pcgstate[i], time(NULL) + (i + 1) * 10, 1);
    };

    s->time_0 = s->time;

    printf("INFO: Take initial condition from configuration file %s.\n", s->conf_file_name);
    read_conf_arrays(s);

    calc_cell_neighbor(s);

    for(int i = 0; i < n_beads; i++)
    {
        s->fx[i] = 0.;
        s->fy[i] = 0.;
        s->fz[i] = 0.;

        s->rx0[i] = s->rx_check[i] = s->rx[i];
        s->ry0[i] = s->ry_check[i] = s->ry[i];
        s->rz0[i] = s->rz_check[i] = s->rz[i];

        s->imx0[i] = s->imx_check[i] = s->imx[i];
        s->imy0[i] = s->imy_check[i] = s->imy[i];
        s->imz0[i] = s->imz_check[i] = s->imz[i];

        s->id_cell[i] = id_cell(s, i);
        s->id_orig[i] = i;
    }

    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;

    for(int i = 0; i < s->n_beads; i++)
    {
        sum_x += s->px[i];
        sum_y += s->py[i];
        sum_z += s->pz[i];
    }

    sum_x /= n_beads;
    sum_y /= n_beads;
    sum_z /= n_beads;

    for(int i = 0; i < s->n_beads; i++)
    {
        s->px[i] -= sum_x;
        s->py[i] -= sum_y;
        s->pz[i] -= sum_z;
    }

    #ifdef _OPENMP
    #pragma omp parallel
    {
        if(omp_get_thread_num() == 0)
        {
            printf("Running on 1 CPU with %d OpenMP threads.\n", omp_get_num_threads());
        }
    }
    #endif

    #ifdef _OPENACC
    const int ngpus = acc_get_num_devices(acc_device_nvidia);
    if(ngpus)
    {
        printf("Running on 1 CPU accelerated by %d Nvidia GPU.\n", ngpus);
    }
    else
    {
        printf("WARNING: No Nvidia GPU found for accceleration! Running on 1 CPU with one thread.\n");
    }
    #endif



    if(access(s->ana_file_name, F_OK))
    {
        char str[1048576];

        printf("Analyzing the following quantities and save them to %s:\n", s->ana_file_name);

        sprintf(str, "[0]time [1]e_kin [2]e_bl [3]e_nl [4]e_tot [5]e_delta/e_tot [6]px_tot [7]py_tot [8]pz_tot [9]T_kin [10]T_conf [11]T_conf_alt [12]cl_mean [13]cl_std [14]nl_mean [15]nl_std [16]n_nl_rebuild [17]bead_msd [18]bead_msd_x [19]bead_msd_y [20]bead_msd_z [21]poly_msd [22]poly_msd_x [23]poly_msd_y [24]poly_msd_z [25]re2 [26]re2_x [27]re2_y [28]re2_z [29]rg2 [30]rg2_x [31]rg2_y [32]rg2_z [33]lc\n");

        printf("%s", str);

        FILE * f = fopen(s->ana_file_name, "w");
        fprintf(f, "%s", str);
        fclose(f);
    }

    #pragma acc enter data copyin(s[0:1])
    #pragma acc enter data copyin(s->rx[0:s->n_beads])
    #pragma acc enter data copyin(s->ry[0:s->n_beads])
    #pragma acc enter data copyin(s->rz[0:s->n_beads])
    #pragma acc enter data copyin(s->rx0[0:s->n_beads])
    #pragma acc enter data copyin(s->ry0[0:s->n_beads])
    #pragma acc enter data copyin(s->rz0[0:s->n_beads])
    #pragma acc enter data copyin(s->rx_check[0:s->n_beads])
    #pragma acc enter data copyin(s->ry_check[0:s->n_beads])
    #pragma acc enter data copyin(s->rz_check[0:s->n_beads])
    #pragma acc enter data copyin(s->px[0:s->n_beads])
    #pragma acc enter data copyin(s->py[0:s->n_beads])
    #pragma acc enter data copyin(s->pz[0:s->n_beads])
    #pragma acc enter data copyin(s->fx[0:s->n_beads])
    #pragma acc enter data copyin(s->fy[0:s->n_beads])
    #pragma acc enter data copyin(s->fz[0:s->n_beads])
    #pragma acc enter data copyin(s->imx[0:s->n_beads])
    #pragma acc enter data copyin(s->imy[0:s->n_beads])
    #pragma acc enter data copyin(s->imz[0:s->n_beads])
    #pragma acc enter data copyin(s->imx0[0:s->n_beads])
    #pragma acc enter data copyin(s->imy0[0:s->n_beads])
    #pragma acc enter data copyin(s->imz0[0:s->n_beads])
    #pragma acc enter data copyin(s->imx_check[0:s->n_beads])
    #pragma acc enter data copyin(s->imy_check[0:s->n_beads])
    #pragma acc enter data copyin(s->imz_check[0:s->n_beads])
    #pragma acc enter data copyin(s->T_k[0:s->n_beads])
    #pragma acc enter data copyin(s->F2[0:s->n_beads])
    #pragma acc enter data copyin(s->nablaF[0:s->n_beads])
    #pragma acc enter data copyin(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc enter data copyin(s->bl_len[0:s->n_beads])
    #pragma acc enter data copyin(s->nl[0:s->n_beads*27*s->cl_len_max])
    #pragma acc enter data copyin(s->nl_len[0:s->n_beads])
    #pragma acc enter data copyin(s->cell_neighbor[0:27*s->n_cells])
    #pragma acc enter data copyin(s->poly_offset[0:s->n_polys])
    #pragma acc enter data copyin(s->poly_N[0:s->n_polys])
    #pragma acc enter data copyin(s->pcgstate[0:s->n_rngs])
    #pragma acc enter data copyin(s->id_cell[0:s->n_beads])

    #pragma acc enter data copyin(s->cell_begin[0:s->n_cells])
    #pragma acc enter data copyin(s->cell_end[0:s->n_cells])
    #pragma acc enter data copyin(s->id_orig[0:s->n_beads])
    #pragma acc enter data copyin(s->id_orig_rev[0:s->n_beads])
    #pragma acc enter data copyin(s->id_sort[0:s->n_beads])
    #pragma acc enter data copyin(s->id_unsort[0:s->n_beads])

    #pragma acc enter data copyin(s->rx_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->ry_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->rz_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->rx0_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->ry0_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->rz0_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->px_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->py_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->pz_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->fx_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->fy_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->fz_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->imx_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->imy_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->imz_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->imx0_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->imy0_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->imz0_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->bl_copy[0:s->n_beads*s->bl_len_max])
    #pragma acc enter data copyin(s->bl_len_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->id_cell_copy[0:s->n_beads])
    #pragma acc enter data copyin(s->id_orig_copy[0:s->n_beads])

    fill_cl(s);
    fill_nl(s);
    forces(s);

    printf("INFO: Start simulation with %d polymers, %d monomers\n", n_polys, n_beads);
    printf("########################################################################################################################\n");
    return 0;
}

void finalize(MySystem * s)
{
    printf("Simulation done. Clean up.\n");

    #pragma acc exit data copyout(s->rx[0:s->n_beads])
    #pragma acc exit data copyout(s->ry[0:s->n_beads])
    #pragma acc exit data copyout(s->rz[0:s->n_beads])
    #pragma acc exit data copyout(s->rx0[0:s->n_beads])
    #pragma acc exit data copyout(s->ry0[0:s->n_beads])
    #pragma acc exit data copyout(s->rz0[0:s->n_beads])
    #pragma acc exit data copyout(s->rx_check[0:s->n_beads])
    #pragma acc exit data copyout(s->ry_check[0:s->n_beads])
    #pragma acc exit data copyout(s->rz_check[0:s->n_beads])
    #pragma acc exit data copyout(s->px[0:s->n_beads])
    #pragma acc exit data copyout(s->py[0:s->n_beads])
    #pragma acc exit data copyout(s->pz[0:s->n_beads])
    #pragma acc exit data copyout(s->fx[0:s->n_beads])
    #pragma acc exit data copyout(s->fy[0:s->n_beads])
    #pragma acc exit data copyout(s->fz[0:s->n_beads])
    #pragma acc exit data copyout(s->imx[0:s->n_beads])
    #pragma acc exit data copyout(s->imy[0:s->n_beads])
    #pragma acc exit data copyout(s->imz[0:s->n_beads])
    #pragma acc exit data copyout(s->imx0[0:s->n_beads])
    #pragma acc exit data copyout(s->imy0[0:s->n_beads])
    #pragma acc exit data copyout(s->imz0[0:s->n_beads])
    #pragma acc exit data copyout(s->imx_check[0:s->n_beads])
    #pragma acc exit data copyout(s->imy_check[0:s->n_beads])
    #pragma acc exit data copyout(s->imz_check[0:s->n_beads])
    #pragma acc exit data copyout(s->T_k[0:s->n_beads])
    #pragma acc exit data copyout(s->F2[0:s->n_beads])
    #pragma acc exit data copyout(s->nablaF[0:s->n_beads])
    #pragma acc exit data copyout(s->bl[0:s->n_beads*s->bl_len_max])
    #pragma acc exit data copyout(s->bl_len[0:s->n_beads])
    #pragma acc exit data copyout(s->nl[0:s->n_beads*27*s->cl_len_max])
    #pragma acc exit data copyout(s->nl_len[0:s->n_beads])
    #pragma acc exit data copyout(s->cell_neighbor[0:27*s->n_cells])
    #pragma acc exit data copyout(s->poly_offset[0:s->n_polys])
    #pragma acc exit data copyout(s->poly_N[0:s->n_polys])
    #pragma acc exit data copyout(s->pcgstate[0:s->n_rngs])
    #pragma acc exit data copyout(s->id_cell[0:s->n_beads])

    #pragma acc exit data copyout(s->cell_begin[0:s->n_cells])
    #pragma acc exit data copyout(s->cell_end[0:s->n_cells])
    #pragma acc exit data copyout(s->id_orig[0:s->n_beads])
    #pragma acc exit data copyout(s->id_orig_rev[0:s->n_beads])
    #pragma acc exit data copyout(s->id_sort[0:s->n_beads])
    #pragma acc exit data copyout(s->id_unsort[0:s->n_beads])

    #pragma acc exit data copyout(s->rx_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->ry_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->rz_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->rx0_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->ry0_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->rz0_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->px_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->py_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->pz_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->fx_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->fy_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->fz_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->imx_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->imy_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->imz_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->imx0_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->imy0_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->imz0_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->bl_copy[0:s->n_beads*s->bl_len_max])
    #pragma acc exit data copyout(s->bl_len_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->id_cell_copy[0:s->n_beads])
    #pragma acc exit data copyout(s->id_orig_copy[0:s->n_beads])

    #pragma acc exit data copyout(s[0:1])

    free(s->rx);
    free(s->ry);
    free(s->rz);
    free(s->imx);
    free(s->imy);
    free(s->imz);
    free(s->px);
    free(s->py);
    free(s->pz);
    free(s->fx);
    free(s->fy);
    free(s->fz);
    free(s->rx0);
    free(s->ry0);
    free(s->rz0);
    free(s->imx0);
    free(s->imy0);
    free(s->imz0);
    free(s->rx_check);
    free(s->ry_check);
    free(s->rz_check);
    free(s->imx_check);
    free(s->imy_check);
    free(s->imz_check);
    free(s->nl);
    free(s->bl);
    free(s->nl_len);
    free(s->bl_len);
    free(s->cell_neighbor);
    free(s->poly_offset);
    free(s->poly_N);
    free(s->pcgstate);
    free(s->T_k);
    free(s->F2);
    free(s->nablaF);
    free(s->id_cell);

    free(s->cell_begin);
    free(s->cell_end);
    free(s->id_orig);
    free(s->id_orig_rev);
    free(s->id_sort);
    free(s->id_unsort);

    free(s->rx_copy);
    free(s->ry_copy);
    free(s->rz_copy);
    free(s->imx_copy);
    free(s->imy_copy);
    free(s->imz_copy);
    free(s->px_copy);
    free(s->py_copy);
    free(s->pz_copy);
    free(s->fx_copy);
    free(s->fy_copy);
    free(s->fz_copy);
    free(s->rx0_copy);
    free(s->ry0_copy);
    free(s->rz0_copy);
    free(s->imx0_copy);
    free(s->imy0_copy);
    free(s->imz0_copy);
    free(s->bl_copy);
    free(s->bl_len_copy);
    free(s->id_cell_copy);
    free(s->id_orig_copy);
}

void read_conf_arrays(MySystem * s)
{
    hid_t file_id = H5Fopen(s->conf_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_dataset(file_id, s->rx, "rx", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->ry, "ry", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->rz, "rz", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->imx, "imx", H5T_NATIVE_INT);
    read_dataset(file_id, s->imy, "imy", H5T_NATIVE_INT);
    read_dataset(file_id, s->imz, "imz", H5T_NATIVE_INT);
    read_dataset(file_id, s->px, "px", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->py, "py", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->pz, "pz", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, s->bl, "bl", H5T_NATIVE_INT);
    read_dataset(file_id, s->bl_len, "bl_len", H5T_NATIVE_INT);
    read_dataset(file_id, s->poly_offset, "poly_offset", H5T_NATIVE_INT);
    read_dataset(file_id, s->poly_N, "poly_N", H5T_NATIVE_INT);

    H5Fclose(file_id);
}

void read_conf_scalars(MySystem * s)
{
    hid_t file_id = H5Fopen(s->conf_file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    read_dataset(file_id, &s->n_beads, "n_beads", H5T_NATIVE_INT);
    read_dataset(file_id, &s->n_polys, "n_polys", H5T_NATIVE_INT);
    read_dataset(file_id, &s->bl_len_max, "bl_len_max", H5T_NATIVE_INT);
    read_dataset(file_id, &s->Lx, "Lx", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->Ly, "Ly", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->Lz, "Lz", H5T_NATIVE_DOUBLE);
    read_dataset(file_id, &s->time, "time", H5T_NATIVE_DOUBLE);

    H5Fclose(file_id);
}

void save_conf(MySystem * s)
{
    char fn[1024];
    hsize_t d1[1] = {1}, dm[1] = {s->n_beads}, dp[1] = {s->n_polys}, dbl[2] = {s->n_beads, s->bl_len_max};

    sprintf(fn, "%s_conf_t%lg.h5", s->prefix, s->time);
    hid_t file_id = H5Fcreate(fn, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        int i_sort = s->id_orig_rev[i];
        s->rx_copy[i] = s->rx[i_sort];
        s->ry_copy[i] = s->ry[i_sort];
        s->rz_copy[i] = s->rz[i_sort];
        s->imx_copy[i] = s->imx[i_sort];
        s->imy_copy[i] = s->imy[i_sort];
        s->imz_copy[i] = s->imz[i_sort];
        s->px_copy[i] = s->px[i_sort];
        s->py_copy[i] = s->py[i_sort];
        s->pz_copy[i] = s->pz[i_sort];
        s->bl_len_copy[i] = s->bl_len[i_sort];

        #pragma acc loop seq
        for(int b0 = 0; b0 < s->bl_len[i_sort]; b0++)
        {
            int b_sort = s->bl[i_sort * s->bl_len_max + b0];
            int b_orig = s->id_orig[b_sort];
            s->bl_copy[i * s->bl_len_max + b0] = b_orig;
        }
    }

    #pragma acc update host(s->rx_copy[0:s->n_beads])
    #pragma acc update host(s->ry_copy[0:s->n_beads])
    #pragma acc update host(s->rz_copy[0:s->n_beads])
    #pragma acc update host(s->px_copy[0:s->n_beads])
    #pragma acc update host(s->py_copy[0:s->n_beads])
    #pragma acc update host(s->pz_copy[0:s->n_beads])
    #pragma acc update host(s->imx_copy[0:s->n_beads])
    #pragma acc update host(s->imy_copy[0:s->n_beads])
    #pragma acc update host(s->imz_copy[0:s->n_beads])
    #pragma acc update host(s->bl_len_copy[0:s->n_beads])
    #pragma acc update host(s->bl_copy[0:s->n_beads*s->bl_len_max])

    write_dataset(file_id, s->rx_copy, "rx", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->ry_copy, "ry", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->rz_copy, "rz", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->imx_copy, "imx", 1, dm, H5T_NATIVE_INT);
    write_dataset(file_id, s->imy_copy, "imy", 1, dm, H5T_NATIVE_INT);
    write_dataset(file_id, s->imz_copy, "imz", 1, dm, H5T_NATIVE_INT);
    write_dataset(file_id, s->px_copy, "px", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->py_copy, "py", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->pz_copy, "pz", 1, dm, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, s->bl_copy, "bl", 2, dbl, H5T_NATIVE_INT);
    write_dataset(file_id, s->bl_len_copy, "bl_len", 1, dm, H5T_NATIVE_INT);
    write_dataset(file_id, s->poly_offset, "poly_offset", 1, dp, H5T_NATIVE_INT);
    write_dataset(file_id, s->poly_N, "poly_N", 1, dp, H5T_NATIVE_INT);
    write_dataset(file_id, &s->n_beads, "n_beads", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->n_polys, "n_polys", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->bl_len_max, "bl_len_max", 1, d1, H5T_NATIVE_INT);
    write_dataset(file_id, &s->Lx, "Lx", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->Ly, "Ly", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->Lz, "Lz", 1, d1, H5T_NATIVE_DOUBLE);
    write_dataset(file_id, &s->time, "time", 1, d1, H5T_NATIVE_DOUBLE);

    H5Fclose(file_id);
}

void write_dataset(hid_t file_id, void *data, char *name, int rank, hsize_t * current_dims, hid_t dtype_id)
{
    hid_t dataspace_id = H5Screate_simple(rank, current_dims, NULL);
    hid_t dataset_id = H5Dcreate2(file_id, name, dtype_id, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
}

void read_dataset(hid_t file_id, void *data, char *name, hid_t dtype_id)
{
    hid_t dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
    H5Dread(dataset_id, dtype_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
}

void write_simulation_data(MySystem * s)
{
    long t = (long)nearbyintl(s->time / s->dt);

    if(t % s->conf_save_dt_int == 0 && s->time_step > 0)
    {
        save_conf(s);
    }

    if(t % s->ana_dt_int == 0)
    {
        ana(s);
    }
}

int print_info(MySystem * s, struct timespec time_ref)
{
    struct timespec time_current;
    static struct timespec time_last = {0, 0};
    static int t_last = 0;
    double elapsed_time, tps;

    clock_gettime(CLOCK_REALTIME, &time_current);
    time_current.tv_sec -= time_ref.tv_sec;
    time_current.tv_nsec -= time_ref.tv_nsec;
    elapsed_time = time_current.tv_sec - time_last.tv_sec + (time_current.tv_nsec - time_last.tv_nsec) / 1e9;
    if(elapsed_time > s->info_dt)
    {
        tps = (s->time_step - t_last) * s->dt / elapsed_time;
        printf("INFO: Running for %d s | Simulation time: %lg (%lg %%) | TPS = %lg\n", (int) (time_current.tv_sec), s->time, (double) s->time_step / s->sim_time_int * 100, tps);
        t_last = s->time_step;
        time_last.tv_sec = time_current.tv_sec;
        time_last.tv_nsec = time_current.tv_nsec;
    }
    return (int)time_current.tv_sec;
}
