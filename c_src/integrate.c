#include "system.h"
#include "integrate.h"
#include "pcg.h"
#include "mesh.h"
#include "potentials.h"
#include "sort.h"

void sort(int *d_key, int *d_values, int num, void *stream);

void integrate(MySystem * s)
{
    vv_step_one(s);
    vv_step_two(s);
    s->time_step++;
    s->time = s->time_0 + s->dt * s->time_step;
}

void vv_step_one(MySystem * s)
{
    double max_dr2 = 0;

    #pragma omp parallel for reduction(max:max_dr2)
    #pragma acc parallel loop present(s[0:1]) reduction(max:max_dr2)
    for(int i = 0; i < s->n_beads; i++)
    {
        s->rx[i] += (s->px[i] + s->fx[i] * s->dt * 0.5) * s->dt;
        s->ry[i] += (s->py[i] + s->fy[i] * s->dt * 0.5) * s->dt;
        s->rz[i] += (s->pz[i] + s->fz[i] * s->dt * 0.5) * s->dt;
        s->px[i] += s->fx[i] * 0.5 * s->dt;
        s->py[i] += s->fy[i] * 0.5 * s->dt;
        s->pz[i] += s->fz[i] * 0.5 * s->dt;
        const int dimx = (int)floor(s->rx[i] / s->Lx);
        const int dimy = (int)floor(s->ry[i] / s->Ly);
        const int dimz = (int)floor(s->rz[i] / s->Lz);
        s->imx[i] += dimx;
        s->imy[i] += dimy;
        s->imz[i] += dimz;
        s->rx[i] -= dimx * s->Lx;
        s->ry[i] -= dimy * s->Ly;
        s->rz[i] -= dimz * s->Lz;

        const double dx = s->rx[i] - s->rx_check[i] + (s->imx[i] - s->imx_check[i]) * s->Lx;
        const double dy = s->ry[i] - s->ry_check[i] + (s->imy[i] - s->imy_check[i]) * s->Ly;
        const double dz = s->rz[i] - s->rz_check[i] + (s->imz[i] - s->imz_check[i]) * s->Lz;
        const double dr2 = dx * dx + dy * dy + dz * dz;

        if(dr2 > max_dr2)
        {
            max_dr2 = dr2;
        }
    }

    if(max_dr2 > 0.25 * s->r_buffer2)
    {
        s->n_nl_rebuild++;
        fill_cl(s);
        fill_nl(s);
    }
}

void vv_step_two(MySystem * s)
{
    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        double fx = 0;
        double fy = 0;
        double fz = 0;

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            const int j = s->bl[i * s->bl_len_max + j0];

            const double dx = s->rx[i] - s->rx[j] + (s->imx[i] - s->imx[j]) * s->Lx;
            const double dy = s->ry[i] - s->ry[j] + (s->imy[i] - s->imy[j]) * s->Ly;
            const double dz = s->rz[i] - s->rz[j] + (s->imz[i] - s->imz[j]) * s->Lz;
            const double r2 = dx * dx + dy * dy + dz * dz;

            const double f = -fene_potential_del_1_div_r(r2);
            fx += f * dx;
            fy += f * dy;
            fz += f * dz;
        }

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            const int j = s->nl[i * 27 * s->cl_len_max + j0];
            const double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->Lx);
            const double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->Ly);
            const double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->Lz);
            const double r2 = dx * dx + dy * dy + dz * dz;
            if(r2 < s->r_cut2)
            {
                const double f = -lj_potential_del_1_div_r(r2);
                fx += f * dx;
                fy += f * dy;
                fz += f * dz;
            }
        }

        s->fx[i] = fx;
        s->fy[i] = fy;
        s->fz[i] = fz;

        s->px[i] += 0.5 * s->fx[i] * s->dt;
        s->py[i] += 0.5 * s->fy[i] * s->dt;
        s->pz[i] += 0.5 * s->fz[i] * s->dt;
    }
}

void fill_cl(MySystem * s)
{
    sort_system(s);

    int max = 0;
    #pragma omp parallel for reduction(max:max)
    #pragma acc parallel loop present(s[0:1]) reduction(max:max)
    for(int c = 0; c < s->n_cells; c++)
    {
        const int cl_len = s->cell_end[c] - s->cell_begin[c];
        if(cl_len > max)
        {
            max = cl_len;
        }
    }

    if(max > s->cl_len_max)
    {
        #pragma acc exit data delete(s->nl[0:s->n_beads*27*s->cl_len_max])
        s->cl_len_max = max;
        #pragma acc update device(s->cl_len_max)
        free(s->nl);
        s->nl = (int*)malloc(s->n_beads * 27 * s->cl_len_max * sizeof(int));
        #pragma acc enter data create(s->nl[0:s->n_beads*27*s->cl_len_max])
    }
}

void fill_nl(MySystem * s)
{
    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int c = s->id_cell[i];

        int n = 0;

        #pragma acc loop seq
        for(int cn0 = 0; cn0 < 27; cn0++)
        {
            const int cn = s->cell_neighbor[c * 27 + cn0];

            #pragma acc loop seq
            for(int j = s->cell_begin[cn]; j < s->cell_end[cn]; j++)
            {
                const double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->Lx);
                const double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->Ly);
                const double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->Lz);
                const double r2 = dx * dx + dy * dy + dz * dz;

                if(r2 < s->r_nl2 && r2 > 1e-10)
                {
                    s->nl[i * 27 * s->cl_len_max + n] = j;
                    n++;
                }
            }
        }
        s->nl_len[i] = n;
    }
}

void forces(MySystem * s)
{
    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        s->fx[i] = 0.;
        s->fy[i] = 0.;
        s->fz[i] = 0.;

        double fx = 0;
        double fy = 0;
        double fz = 0;

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            const int j = s->bl[i * s->bl_len_max + j0];
            const double dx = s->rx[i] - s->rx[j] + (s->imx[i] - s->imx[j]) * s->Lx;
            const double dy = s->ry[i] - s->ry[j] + (s->imy[i] - s->imy[j]) * s->Ly;
            const double dz = s->rz[i] - s->rz[j] + (s->imz[i] - s->imz[j]) * s->Lz;
            const double r2 = dx * dx + dy * dy + dz * dz;

            const double f = -fene_potential_del_1_div_r(r2);
            fx += f * dx;
            fy += f * dy;
            fz += f * dz;

        }

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            const int j = s->nl[i * 27 * s->cl_len_max + j0];
            const double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->Lx);
            const double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->Ly);
            const double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->Lz);
            const double r2 = dx * dx + dy * dy + dz * dz;
            if(r2 < s->r_cut2)
            {
                const double f = -lj_potential_del_1_div_r(r2);
                fx += f * dx;
                fy += f * dy;
                fz += f * dz;
            }
        }

        s->fx[i] += fx;
        s->fy[i] += fy;
        s->fz[i] += fz;
    }
}
