#ifdef _OPENACC
#include <openacc.h>
#include <cuda_runtime.h>
#endif
#include "system.h"
#include "mesh.h"
#include "sort.h"

void sort(int *d_key, int *d_values, int num, void *stream);

void sort_system(MySystem * s)
{
    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1]) async
    for(int c = 0; c < s->n_cells; c++)
    {
        s->cell_begin[c] = -1;
        s->cell_end[c] = -1;
    }

    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1]) async
    for(int i = 0; i < s->n_beads; i++)
    {
        s->id_unsort[i] = i;
        s->id_cell[i] = id_cell(s, i);
    }

    #pragma acc wait

    #ifdef _OPENACC

    void *stream = acc_get_cuda_stream(acc_async_sync);

    #pragma acc host_data use_device(s->id_cell, s->id_unsort)
    {
        sort(s->id_cell, s->id_unsort, s->n_beads, stream);
    }

    #else

    Sort_item * item = (Sort_item*)malloc(s->n_beads * sizeof(Sort_item));

    #pragma omp parallel for
    for(int i = 0; i < s->n_beads; i++)
    {
        item[i].id_cell = s->id_cell[i];
        item[i].id_unsort = s->id_unsort[i];
    }

    qsort(item, s->n_beads, sizeof(Sort_item), compare);

    #pragma omp parallel for
    for(int i = 0; i < s->n_beads; i++)
    {
        s->id_cell[i] = item[i].id_cell;
        s->id_unsort[i] = item[i].id_unsort;
    }

    free(item);

    #endif

    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int j = s->id_unsort[i];
        s->id_sort[j] = i;
        s->id_orig_copy[i] = s->id_orig[j];
        s->rx_copy[i] = s->rx[j];
        s->ry_copy[i] = s->ry[j];
        s->rz_copy[i] = s->rz[j];
        s->px_copy[i] = s->px[j];
        s->py_copy[i] = s->py[j];
        s->pz_copy[i] = s->pz[j];
        s->fx_copy[i] = s->fx[j];
        s->fy_copy[i] = s->fy[j];
        s->fz_copy[i] = s->fz[j];
        s->imx_copy[i] = s->imx[j];
        s->imy_copy[i] = s->imy[j];
        s->imz_copy[i] = s->imz[j];
        s->bl_len_copy[i] = s->bl_len[j];
    }

    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        const int j = s->id_unsort[i];

        #pragma acc loop seq
        for(int b0 = 0; b0 < s->bl_len[j]; b0++)
        {
            const int b_unsort = s->bl[j * s->bl_len_max + b0];
            const int b_sort = s->id_sort[b_unsort];
            s->bl_copy[i * s->bl_len_max + b0] = b_sort;
        }
    }

    #pragma omp parallel for
    #pragma acc parallel loop present(s[0:1])
    for(int i = 0; i < s->n_beads; i++)
    {
        s->id_orig[i] = s->id_orig_copy[i];
        const int j = s->id_orig[i];
        s->id_orig_rev[j] = i;
        s->rx[i] = s->rx_copy[i];
        s->ry[i] = s->ry_copy[i];
        s->rz[i] = s->rz_copy[i];
        s->px[i] = s->px_copy[i];
        s->py[i] = s->py_copy[i];
        s->pz[i] = s->pz_copy[i];
        s->fx[i] = s->fx_copy[i];
        s->fy[i] = s->fy_copy[i];
        s->fz[i] = s->fz_copy[i];
        s->imx[i] = s->imx_copy[i];
        s->imy[i] = s->imy_copy[i];
        s->imz[i] = s->imz_copy[i];
        s->bl_len[i] = s->bl_len_copy[i];

        s->rx_check[i] = s->rx[i];
        s->ry_check[i] = s->ry[i];
        s->rz_check[i] = s->rz[i];
        s->imx_check[i] = s->imx[i];
        s->imy_check[i] = s->imy[i];
        s->imz_check[i] = s->imz[i];

        const int c1 = s->id_cell[i];
        const int i2 = i + 1 < s->n_beads ? i + 1 : i + 1 - s->n_beads;
        const int c2 = s->id_cell[i2];

        if(c1 != c2)
        {
            s->cell_begin[c2] = i2;
            s->cell_end[c1] = i2;
        }

        if(i == s->n_beads - 1)
        {
            s->cell_end[c1] = s->n_beads;
        }

        #pragma acc loop seq
        for(int b0 = 0; b0 < s->bl_len[i]; b0++)
        {
            s->bl[i * s->bl_len_max + b0] = s->bl_copy[i * s->bl_len_max + b0];
        }
    }
}

int compare(const void *s1, const void *s2)
{
  Sort_item *e1 = (Sort_item *)s1;
  Sort_item *e2 = (Sort_item *)s2;
  return e1->id_cell - e2->id_cell;
}
