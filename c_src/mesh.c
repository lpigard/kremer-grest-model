#include <math.h>
#include "system.h"
#include "mesh.h"

void init_mesh(MySystem *s)
{
    const double r_cut = pow(2., 1. / 6);
    const double r_buffer = sqrt(s->r_buffer2);
    s->r_cut2 = r_cut * r_cut;
    const double r_nl = r_cut + r_buffer;
    s->r_nl2 = r_nl * r_nl;
    s->n_nl_rebuild = 0;

    s->nx = (int)(s->Lx / r_nl);
    s->ny = (int)(s->Ly / r_nl);
    s->nz = (int)(s->Lz / r_nl);

    s->lx = s->Lx / s->nx;
    s->ly = s->Ly / s->ny;
    s->lz = s->Lz / s->nz;

    s->n_cells = s->nx * s->ny * s->nz;
}

double wrap_back_dis(double d, double l)
{
    return d - l * nearbyint(d / l);
}

int id_cell(MySystem *s, int i)
{
    const int x = floor(s->rx[i] / s->lx);
    const int y = floor(s->ry[i] / s->ly);
    const int z = floor(s->rz[i] / s->lz);
    return z + s->nz * (y + s->ny * x);
}

void calc_cell_neighbor(MySystem *s)
{
    for(int x = 0; x < s->nx; x++)
    {
        for(int y = 0; y < s->ny; y++)
        {
            for(int z = 0; z < s->nz; z++)
            {
                int c = z + s->nz * (y + s->ny * x);
                int n = 0;
                for(int dx = -1; dx <= 1; dx++)
                {
                    for(int dy = -1; dy <= 1; dy++)
                    {
                        for(int dz = -1; dz <= 1; dz++)
                        {
                            int xn = x + dx;
                            int yn = y + dy;
                            int zn = z + dz;

                            if(xn < 0)
                            {
                                xn += s->nx;
                            }
                            else if(xn >= s->nx)
                            {
                                xn -= s->nx;
                            }

                            if(yn < 0)
                            {
                                yn += s->ny;
                            }
                            else if(yn >= s->ny)
                            {
                                yn -= s->ny;
                            }

                            if(zn < 0)
                            {
                                zn += s->nz;
                            }
                            else if(zn >= s->nz)
                            {
                                zn -= s->nz;
                            }

                            int cn = zn + s->nz * (yn + s->ny * xn);
                            s->cell_neighbor[c * 27 + n] = cn;
                            n++;

                        }
                    }
                }
            }
        }
    }
}
