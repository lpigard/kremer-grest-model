#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "system.h"
#include "io.h"
#include "mesh.h"
#include "integrate.h"
#include "ana.h"
#include "potentials.h"


void ana(MySystem * s)
{
    char str[1048576];

    ana_e_kin(s);
    ana_e_bl(s);
    ana_e_nl(s);
    ana_e_tot(s);
    ana_p_tot(s);
    ana_T_kin(s);
    ana_T_conf(s);
    ana_cl_mean(s);
    ana_cl_std(s);
    ana_nl_mean(s);
    ana_nl_std(s);
    ana_bead_msd(s);
    ana_poly_msd(s);
    ana_re2(s);
    ana_rg2(s);
    ana_lc(s);

    sprintf(str, "%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg\n", s->time, s->e_kin, s->e_bl, s->e_nl, s->e_tot, fabs(s->e_delta) / s->e_tot, s->px_tot / s->n_beads, s->py_tot / s->n_beads, s->pz_tot / s->n_beads, s->T_kin, s->T_conf, s->T_conf_alt, s->cl_mean, s->cl_std, s->nl_mean, s->nl_std, (double)s->n_nl_rebuild, s->bead_msd, s->bead_msd_x, s->bead_msd_y, s->bead_msd_z, s->poly_msd, s->poly_msd_x, s->poly_msd_y, s->poly_msd_z, s->re2, s->re2_x, s->re2_y, s->re2_z, s->rg2, s->rg2_x, s->rg2_y, s->rg2_z, s->lc);

    s->n_nl_rebuild = 0;

    // printf(str);

    FILE * f = fopen(s->ana_file_name, "a");
    fprintf(f, "%s", str);
    fclose(f);
}

void ana_e_tot(MySystem * s)
{
    static double e_tot_old = 0.;
    e_tot_old = s->e_tot;
    s->e_tot = s->e_kin + s->e_bl + s->e_nl;
    s->e_delta = s->e_tot - e_tot_old;
}

void ana_e_kin(MySystem * s)
{
    double sum = 0.;
    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        sum += s->px[i] * s->px[i] + s->py[i] * s->py[i] + s->pz[i] * s->pz[i];
    }
    s->e_kin = 0.5 * sum;
}

void ana_e_bl(MySystem * s)
{
    double sum = 0.;
    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            int j = s->bl[i * s->bl_len_max + j0];
            if(i < j)
            {
                const double dx = s->rx[i] - s->rx[j] + (s->imx[i] - s->imx[j]) * s->Lx;
                const double dy = s->ry[i] - s->ry[j] + (s->imy[i] - s->imy[j]) * s->Ly;
                const double dz = s->rz[i] - s->rz[j] + (s->imz[i] - s->imz[j]) * s->Lz;
                const double r2 = dx * dx + dy * dy + dz * dz;
                sum += fene_potential(r2);
            }
        }
    }
    s->e_bl = sum;
}

void ana_e_nl(MySystem * s)
{
    double sum = 0.;
    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            int j = s->nl[i * 27 * s->cl_len_max + j0];
            if(i < j)
            {
                const double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->Lx);
                const double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->Ly);
                const double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->Lz);
                const double r2 = dx * dx + dy * dy + dz * dz;
                if(r2 < s->r_cut2)
                {
                    sum += lj_potential(r2);
                }
            }
        }
    }
    s->e_nl = sum;
}

void ana_p_tot(MySystem * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;
    #pragma omp parallel for reduction(+:sum_x,sum_y,sum_z)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum_x,sum_y,sum_z)
    for(int i = 0; i < s->n_beads; i++)
    {
        sum_x += s->px[i];
        sum_y += s->py[i];
        sum_z += s->pz[i];
    }
    s->px_tot = sum_x;
    s->py_tot = sum_y;
    s->pz_tot = sum_z;
}

void ana_T_conf(MySystem * s)
{
    double num = 0;
    double den = 0;
    double sum = 0;

    #pragma omp parallel for reduction(+:num,den,sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:num,den,sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        double del_2 = 0;

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->bl_len[i]; j0++)
        {
            int j = s->bl[i * s->bl_len_max + j0];

            const double dx = s->rx[i] - s->rx[j] + (s->imx[i] - s->imx[j]) * s->Lx;
            const double dy = s->ry[i] - s->ry[j] + (s->imy[i] - s->imy[j]) * s->Ly;
            const double dz = s->rz[i] - s->rz[j] + (s->imz[i] - s->imz[j]) * s->Lz;
            const double r2 = dx * dx + dy * dy + dz * dz;
            del_2 += fene_potential_del_2(r2) + 2 * fene_potential_del_1_div_r(r2);
        }

        #pragma acc loop seq
        for(int j0 = 0; j0 < s->nl_len[i]; j0++)
        {
            int j = s->nl[i * 27 * s->cl_len_max + j0];
            const double dx = wrap_back_dis(s->rx[i] - s->rx[j], s->Lx);
            const double dy = wrap_back_dis(s->ry[i] - s->ry[j], s->Ly);
            const double dz = wrap_back_dis(s->rz[i] - s->rz[j], s->Lz);
            const double r2 = dx * dx + dy * dy + dz * dz;
            if(r2 < s->r_cut2)
            {
                del_2 += lj_potential_del_2(r2) + 2 * lj_potential_del_1_div_r(r2);
            }
        }

        double f2 = s->fx[i] * s->fx[i] + s->fy[i] * s->fy[i] + s->fz[i] * s->fz[i];

        s->F2[i] = f2;
        s->nablaF[i] = del_2;
        sum += f2 / del_2;

        num += f2;
        den += del_2;
    }

    s->T_conf = num / den;
    s->T_conf_alt = sum / s->n_beads;
}

void ana_T_kin(MySystem * s)
{
    double sum = 0.;

    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        double T_k = (s->px[i] * s->px[i] + s->py[i] * s->py[i] + s->pz[i] * s->pz[i]) / 3;
        sum += T_k;
        s->T_k[i] = T_k;
    }

    s->T_kin = sum / (s->n_beads - 1);
}

void ana_cl_mean(MySystem * s)
{
    double sum = 0.;
    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int c = 0; c < s->n_cells; c++)
    {
        sum += s->cell_end[c] - s->cell_begin[c];
    }
    s->cl_mean = sum / s->n_cells;
}

void ana_cl_std(MySystem * s)
{
    double sum = 0.;

    #pragma acc update device(s->cl_mean)

    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int c = 0; c < s->n_cells; c++)
    {
        double tmp = s->cell_end[c] - s->cell_begin[c] - s->cl_mean;
        sum += tmp * tmp;
    }
    s->cl_std = sqrt(sum / s->n_cells);
}

void ana_nl_mean(MySystem * s)
{
    double sum = 0.;

    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        sum += s->nl_len[i];
    }
    s->nl_mean = sum / s->n_beads;
}

void ana_nl_std(MySystem * s)
{
    double sum = 0.;

    #pragma acc update device(s->nl_mean)

    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int i = 0; i < s->n_beads; i++)
    {
        double tmp =  s->nl_len[i] - s->nl_mean;
        sum += tmp * tmp;
    }
    s->nl_std = sqrt(sum / s->n_beads);
}

void ana_bead_msd(MySystem * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;
    #pragma omp parallel for reduction(+:sum_x,sum_y,sum_z)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum_x,sum_y,sum_z)
    for(int i = 0; i < s->n_beads; i++)
    {
        int i_sort = s->id_orig_rev[i];

        double dx = s->rx[i_sort] - s->rx0[i] + (s->imx[i_sort] - s->imx0[i]) * s->Lx;
        double dy = s->ry[i_sort] - s->ry0[i] + (s->imy[i_sort] - s->imy0[i]) * s->Ly;
        double dz = s->rz[i_sort] - s->rz0[i] + (s->imz[i_sort] - s->imz0[i]) * s->Lz;

        sum_x += dx * dx;
        sum_y += dy * dy;
        sum_z += dz * dz;
    }
    s->bead_msd_x = sum_x / s->n_beads;
    s->bead_msd_y = sum_y / s->n_beads;
    s->bead_msd_z = sum_z / s->n_beads;
    s->bead_msd = s->bead_msd_x + s->bead_msd_y + s->bead_msd_z;
}

void ana_poly_msd(MySystem * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;
    #pragma omp parallel for reduction(+:sum_x,sum_y,sum_z)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum_x,sum_y,sum_z)
    for(int p = 0; p < s->n_polys; p++)
    {
        double cm_x = 0.;
        double cm_y = 0.;
        double cm_z = 0.;

        int offset = s->poly_offset[p];
        int N = s->poly_N[p];

        #pragma acc loop seq
        for(int i0 = 0; i0 < N; i0++)
        {
            int i = offset + i0;
            int i_sort = s->id_orig_rev[i];

            cm_x += s->rx[i_sort] - s->rx0[i] + (s->imx[i_sort] - s->imx0[i]) * s->Lx;
            cm_y += s->ry[i_sort] - s->ry0[i] + (s->imy[i_sort] - s->imy0[i]) * s->Ly;
            cm_z += s->rz[i_sort] - s->rz0[i] + (s->imz[i_sort] - s->imz0[i]) * s->Lz;
        }

        double dx = cm_x / N;
        double dy = cm_y / N;
        double dz = cm_z / N;

        sum_x += dx * dx;
        sum_y += dy * dy;
        sum_z += dz * dz;
    }
    s->poly_msd_x = sum_x / s->n_polys;
    s->poly_msd_y = sum_y / s->n_polys;
    s->poly_msd_z = sum_z / s->n_polys;
    s->poly_msd = s->poly_msd_x + s->poly_msd_y + s->poly_msd_z;
}

void ana_re2(MySystem * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;
    #pragma omp parallel for reduction(+:sum_x,sum_y,sum_z)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum_x,sum_y,sum_z)
    for(int p = 0; p < s->n_polys; p++)
    {
        int offset = s->poly_offset[p];
        int N = s->poly_N[p];

        int i1 = s->id_orig_rev[offset];
        int i2 = s->id_orig_rev[offset + N - 1];

        double dx = s->rx[i2] - s->rx[i1] + (s->imx[i2] - s->imx[i1]) * s->Lx;
        double dy = s->ry[i2] - s->ry[i1] + (s->imy[i2] - s->imy[i1]) * s->Ly;
        double dz = s->rz[i2] - s->rz[i1] + (s->imz[i2] - s->imz[i1]) * s->Lz;

        sum_x += dx * dx;
        sum_y += dy * dy;
        sum_z += dz * dz;
    }
    s->re2_x = sum_x / s->n_polys;
    s->re2_y = sum_y / s->n_polys;
    s->re2_z = sum_z / s->n_polys;
    s->re2 = s->re2_x + s->re2_y + s->re2_z;
}

void ana_rg2(MySystem * s)
{
    double sum_x = 0.;
    double sum_y = 0.;
    double sum_z = 0.;
    #pragma omp parallel for reduction(+:sum_x,sum_y,sum_z)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum_x,sum_y,sum_z)
    for(int p = 0; p < s->n_polys; p++)
    {
        double cm_x = 0.;
        double cm_y = 0.;
        double cm_z = 0.;

        const int offset = s->poly_offset[p];
        const int N = s->poly_N[p];

        #pragma acc loop seq
        for(int i = offset; i < offset + N; i++)
        {
            const int i_sort = s->id_orig_rev[i];

            cm_x += s->rx[i_sort] + s->imx[i_sort] * s->Lx;
            cm_y += s->ry[i_sort] + s->imy[i_sort] * s->Ly;
            cm_z += s->rz[i_sort] + s->imz[i_sort] * s->Lz;
        }

        cm_x /= N;
        cm_y /= N;
        cm_z /= N;

        double s_x = 0.;
        double s_y = 0.;
        double s_z = 0.;

        #pragma acc loop seq
        for(int i = offset; i < offset + N; i++)
        {
            const int i_sort = s->id_orig_rev[i];

            const double dx = s->rx[i_sort] + s->imx[i_sort] * s->Lx - cm_x;
            const double dy = s->ry[i_sort] + s->imy[i_sort] * s->Ly - cm_y;
            const double dz = s->rz[i_sort] + s->imz[i_sort] * s->Lz - cm_z;

            s_x += dx * dx;
            s_y += dy * dy;
            s_z += dz * dz;
        }

        sum_x += s_x / N;
        sum_y += s_y / N;
        sum_z += s_z / N;
    }
    s->rg2_x = sum_x / s->n_polys;
    s->rg2_y = sum_y / s->n_polys;
    s->rg2_z = sum_z / s->n_polys;
    s->rg2 = s->rg2_x + s->rg2_y + s->rg2_z;
}

void ana_lc(MySystem * s)
{
    double sum = 0.;
    #pragma omp parallel for reduction(+:sum)
    #pragma acc parallel loop present(s[0:1]) reduction(+:sum)
    for(int p = 0; p < s->n_polys; p++)
    {
        const int offset = s->poly_offset[p];
        const int N = s->poly_N[p];

        double sum_p = 0;

        #pragma acc loop seq
        for(int i = offset; i < offset + N - 1; i++)
        {
            int i1 = s->id_orig_rev[i + 1];
            int i2 = s->id_orig_rev[i];

            double dx = s->rx[i2] - s->rx[i1] + (s->imx[i2] - s->imx[i1]) * s->Lx;
            double dy = s->ry[i2] - s->ry[i1] + (s->imy[i2] - s->imy[i1]) * s->Ly;
            double dz = s->rz[i2] - s->rz[i1] + (s->imz[i2] - s->imz[i1]) * s->Lz;

            sum_p += sqrt(dx * dx + dy * dy + dz * dz);
        }

        sum += sum_p / (N - 1);
    }
    s->lc = sum / s->n_polys;
}
