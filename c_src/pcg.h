#ifndef PCG_H
#define PCG_H
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//! \brief State of the random number generator (PCG)
typedef struct{
    uint64_t state; //!<\brief internal state of the PCG generator
    uint64_t inc;   //!<\brief stream of the PCG generator
    }PCG_STATE;

typedef struct
{
    double r1;
    double r2;
}double2_pcg;

uint32_t pcg32_random(PCG_STATE * rng);

int seed_pcg32(PCG_STATE * rng, uint64_t seed, uint64_t stream);
unsigned int pcg_max(void);
double rng_uniform(PCG_STATE * pcgs);
int rng_uniform_int(int i, PCG_STATE * pcgs);
double2_pcg rng_normal(PCG_STATE * pcgs);

#endif//PCG_H
