#include "pcg.h"

typedef struct MySystem
{
    double * rx;
    double * ry;
    double * rz;
    double * rx0;
    double * ry0;
    double * rz0;
    double * rx_check;
    double * ry_check;
    double * rz_check;
    double * px;
    double * py;
    double * pz;
    double * fx;
    double * fy;
    double * fz;
    double * T_k;
    double * F2;
    double * nablaF;
    int * bl;
    int * bl_len;
    int * nl;
    int * nl_len;
    int * cell_neighbor;
    int * poly_offset;
    int * poly_N;
    int * imx;
    int * imy;
    int * imz;
    int * imx0;
    int * imy0;
    int * imz0;
    int * imx_check;
    int * imy_check;
    int * imz_check;
    int * id_cell;

    int * id_orig;
    int * id_sort;
    int * id_unsort;
    int * cell_begin;
    int * cell_end;
    int * id_orig_rev;

    double * rx_copy;
    double * ry_copy;
    double * rz_copy;
    double * rx0_copy;
    double * ry0_copy;
    double * rz0_copy;
    double * px_copy;
    double * py_copy;
    double * pz_copy;
    double * fx_copy;
    double * fy_copy;
    double * fz_copy;
    int * bl_copy;
    int * bl_len_copy;
    int * imx_copy;
    int * imy_copy;
    int * imz_copy;
    int * imx0_copy;
    int * imy0_copy;
    int * imz0_copy;
    int * id_cell_copy;
    int * id_orig_copy;

    double bead_msd;
    double bead_msd_x;
    double bead_msd_y;
    double bead_msd_z;
    double poly_msd;
    double poly_msd_x;
    double poly_msd_y;
    double poly_msd_z;
    double re2;
    double re2_x;
    double re2_y;
    double re2_z;
    double rg2;
    double rg2_x;
    double rg2_y;
    double rg2_z;

    char * prefix;
    char * conf_file_name;
    char * ana_file_name;

    double Lx;
    double Ly;
    double Lz;
    double lx;
    double ly;
    double lz;
    double r_cut2;
    double r_buffer2;
    double r_nl2;
    double dt;
    double info_dt;
    double sim_time_double;
    double conf_save_dt_double;
    double ana_dt_double;
    double time;
    double time_0;
    double e_bl;
    double e_nl;
    double e_kin;
    double e_tot;
    double e_delta;
    double px_tot;
    double py_tot;
    double pz_tot;
    double T_kin;
    double T_conf;
    double T_conf_alt;
    double cl_mean;
    double cl_std;
    double nl_mean;
    double nl_std;
    double lc;

    int nx;
    int ny;
    int nz;
    int n_beads;
    int n_cells;
    int n_polys;
    int sim_time_int;
    int conf_save_dt_int;
    int ana_dt_int;
    int walltime;
    int bl_len_max;
    int cl_len_max;
    int time_step;
    int n_rngs;
    int n_nl_rebuild;

    PCG_STATE pcgstate_main, *pcgs_main;
    PCG_STATE *pcgstate;
} MySystem;
