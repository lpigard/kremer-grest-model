
#pragma acc routine seq
double fene_potential(const double r2);
#pragma acc routine seq
double fene_potential_del_1_div_r(const double r2);
#pragma acc routine seq
double fene_potential_del_2(const double r2);
#pragma acc routine seq
double lj_potential(const double r2);
#pragma acc routine seq
double lj_potential_del_1_div_r(const double r2);
#pragma acc routine seq
double lj_potential_del_2(const double r2);
