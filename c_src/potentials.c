#include <math.h>
#include "potentials.h"

double fene_potential(const double r2)
{
    const double R2 = 2.25;
    return -15. * R2 * log(1 - r2 / R2);
}

double fene_potential_del_1_div_r(const double r2)
{
    const double R2inv = 1 / 2.25;
    return 30. / (1 - r2 * R2inv);
}

double fene_potential_del_2(const double r2)
{
    const double R2 = 2.25;
    return 30. * R2 * (R2 + r2) / ((R2 - r2) * (R2 - r2));
}

double lj_potential(const double r2)
{
    double r6inv = 1. / r2;
    r6inv *= r6inv * r6inv;
    return 4 * (r6inv * r6inv - r6inv + 1. / 4);
}

double lj_potential_del_1_div_r(const double r2)
{
    double r8inv = 1. / r2;
    r8inv *= r8inv;
    r8inv *= r8inv;

    return -4 * r8inv * (12 * r8inv * r2 - 6);
}

double lj_potential_del_2(const double r2)
{
    double r6inv = 1. / r2;
    r6inv *= r6inv * r6inv;
    return 4 * r6inv * (12 * 13 * r6inv - 6 * 7) / r2;
}
